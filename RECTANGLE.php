<?php
/**
 * Created by PhpStorm.
 * User: �����
 * Date: 18.09.2015
 * Time: 21:40
 */
require_once "TRIANGLE.php";

class Rectangle extends TRIANGLE
{
    protected $x4;
    protected $y4;

    public function __construct($x1,$y1,$x2,$y2,$x3,$y3,$x4,$y4){
        parent::__construct($x1,$y1,$x2,$y2,$x3,$y3);
        $this->x4=intval($x4);
        $this->y4=intval($y4);
    }
    public function setX4($x4)
    {
        $this->x4 = intval($x4);
    }

    public function getX4()
    {
        return $this->x4;
    }

    public function setY4($y4)
    {
        $this->y4 = intval($y4);
    }

    public function getY4()
    {
        return $this->y4;
    }
    public function print1(){
        echo "Rectangle is ".$this->getX1()." - ".$this->getY1()." ; ".$this->getX2()." - ".$this->getY2()." ; ".$this->getX3()." - ".$this->getY3()." ; ".$this->getX4()." - ".$this->getY4();
        echo "<br>\n";
    }
}

?>