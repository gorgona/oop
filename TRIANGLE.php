<?php
/**
 * Created by PhpStorm.
 * User: �����
 * Date: 18.09.2015
 * Time: 21:39
 */
require_once "LINE.php";

class TRIANGLE extends LINE
{
    protected $x3;
    protected $y3;

    public function __construct($x1,$y1,$x2,$y2,$x3,$y3){
        parent::__construct($x1,$y1,$x2,$y2);
        $this->x3=intval($x3);
        $this->y3=intval($y3);
    }
    public function setX3($x3)
    {
        $this->x3 = intval($x3);
    }

    public function getX3()
    {
        return $this->x3;
    }

    public function setY3($y3)
    {
        $this->y3 = intval($y3);
    }

    public function getY3()
    {
        return $this->y3;
    }
    public function print1(){
        echo "Triangle is ".$this->getX1()." - ".$this->getY1()." ; ".$this->getX2()." - ".$this->getY2()." ; ".$this->getX3()." - ".$this->getY3();
       echo  "<br>\n";
    }
}

?>